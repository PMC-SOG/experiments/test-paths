#!/bin/bash

# get platform
platform=$(uname)

# sogMBT binary
sog="./tools/sogMBT/${platform}/sogMBT"

# nmodel binary
nmodel="./tools/otg.exe"

# results folder
output_folder="./results"

# folder with the models
models_folder="./models"

# create a folder
function create_folder {
  local path=$1

  if [ ! -d "$path" ]; then
    mkdir -p "$path"
  fi
}

# Print a help message.
function usage() {
  echo "Usage: $0 -m MODEL -b BOUND -t TIMEOUT" 1>&2
}

# Exit with error.
exit_abnormal() {
  usage
  exit 1
}

# run sogMBT
function run_sog {
  local model=$1
  local bound=$2
  local output=$3
  local timeout=$4

  timeout "$timeout" $sog --input-net "$model" --bound "$bound" --output-folder "$output" || echo -e "\ntimeout ${timeout}"
}

# run NModel
function run_nmodel {
  local modelFile=$1
  local modelName=$2
  local timeout=$3

  # factory name
  factory=$(echo $modelName | sed 's/./\L&/g' | sed 's/./\U&/' | sed 's/-/_/')

  # run model
  timeout "$timeout" "mono" "$nmodel" "/r:${modelFile}" "${factory}.Factory.Create" || echo -e "\ntimeout ${timeout}"
}

# Run benchmarks for a model
function run_benchmark {
  local model_name=$1
  local bound=$2
  local timeout=$3

  # create folder to save the outputs
  output="${output_folder}/${model_name}"
  create_folder "${output}"

  # get all the instances of the model (.net files)
  models_files=$(find "$models_folder/${model_name}" -name "*.net" -type f)

  for f in $models_files; do
    file="${f%.*}"
    instance_name="$(basename -- $file)"

    # run sogMBT
    output_sog_file="${output}/${instance_name}.res"
    if [[ ! -f "${output_sog_file}" ]]; then
      echo "running sogMBT (SOG bound ${bound} - timeout ${timeout}): ${file}.net"
      run_sog "${file}.net" "$bound" "$output" "${timeout}" >"${output_sog_file}"
    fi

    # run NModel
    nmodel_file="${output}/${instance_name}ContractTest.txt"
    if [[ -f "${file}.dll" ]] && [[ ! -f "${nmodel_file}" ]]; then
      echo "running NModel (timeout ${timeout}): ${file}.dll"
      (time run_nmodel "${file}.dll" "${instance_name}" "${timeout}") >"${nmodel_file}" 2>&1
    fi
  done
}

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------

TIMEOUT=""
MODEL=""
BOUND=""

while [[ "$#" -gt 0 ]]; do
  case "$1" in
  -m | --model)
    MODEL="$2"
    shift
    ;;
  -b | --bound)
    BOUND="$2"
    shift
    ;;
  -t | --timeout)
    TIMEOUT="$2"
    shift
    ;;
  *) exit_abnormal ;;
  esac
  shift
done

if [ "$MODEL" = "" ] || [ "$BOUND" = "" ] || [ "$TIMEOUT" = "" ]; then
  exit_abnormal
fi

# use "all" for run all the experiments
if [ "$MODEL" = "all" ]; then
  MODEL=$(find "$models_folder" -mindepth 1 -type d -exec basename {} \;)
fi

# run benchmark
for m in $(echo "$MODEL"); do
  if [[ "${m}" = train* ]]; then
    BOUND="2"
  fi
  run_benchmark "${m}" "${BOUND}" "${TIMEOUT}"
done
