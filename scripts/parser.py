import argparse
import json
import os
import re
from collections import defaultdict

import xlrd
from bs4 import BeautifulSoup
from xlutils.copy import copy as xlcopy


def parse_dot_file(filename):
    """Parse a marking graph written in dot file

    Parameters
    ----------
    filename : str
        dot file to be parsed

    Returns
    -------
    dict
        dictionary with keys: states, labels, arcs and initial_state
    """
    with open(filename) as f:
        re_arcs = r"(\w+)\s*->\s*(\w+)\s*\[\s*label\s*=\s*<(\w+)>\];"
        re_initial_state = r"(?P<label>\w+)\s*\[\s*label="

        content = f.read()
        arcs = re.findall(re_arcs, content)

        # get states and labels
        states = set()
        labels = set()
        for (source, target, label) in arcs:
            states.add(source)
            states.add(target)
            labels.add(label)

        # get initial state
        initial_state = re.search(re_initial_state, content).group("label")

        return {
            "states": list(states),
            "labels": list(labels),
            "arcs": arcs,
            "initial_state": initial_state,
        }


def parse_to_graphwalker(
    name, input_file, output_file, generator="random(edge_coverage(100))"
):
    """Parse a marking graph wriiten in dot format into a the graphwalker format.

    Parameters
    ----------
    name : str
        model's name
    input_file : str
        path to the dot file
    output_file : str
        path to the output file
    generator : str, optional
        graphwalker generator, by default "random(edge_coverage(100))"
    """
    with open(output_file, "w") as f:
        graph = parse_dot_file(input_file)

        # parser to graphwalker
        vertices = [
            {"id": s, "name": s, "actions": [], "requirements": []}
            for s in graph["states"]
        ]
        edges = [
            {
                "id": l,
                "name": l,
                "actions": [],
                "requirements": [],
                "properties": [],
                "sourceVertexId": s,
                "targetVertexId": t,
            }
            for (s, t, l) in graph["arcs"]
        ]

        result = {
            "models": [
                {
                    "name": name,
                    "id": name,
                    "generator": generator,
                    "actions": [],
                    "vertices": vertices,
                    "edges": edges,
                    "startElementId": graph["initial_state"],
                }
            ]
        }

        f.write(json.dumps(result, indent=2))


def parse_pnml_file(filename):
    """Parse a PNML file

    Parameters
    ----------
    filename : str
        path to the file to be parsed

    Returns
    -------
    dict
        dictionary with keys: places, transitions and arcs
    """
    with open(filename) as file:
        soup = BeautifulSoup(file, "xml")
        pnml = soup.find("pnml")

        # parsing places
        places = {}
        for p in pnml.find_all("place"):
            # get id
            identifier = p["id"]

            # find label
            label = p.find("name").find("text").text

            # find initial marking
            initialMarking = p.find("initialMarking")
            initialMarking = (
                0 if initialMarking is None else int(initialMarking.find("text").text)
            )
            places[identifier] = {"label": label, "initialMarking": initialMarking}

        # parsing transitions
        transitions = {}
        for t in pnml.find_all("transition"):
            # get id
            identifier = t["id"]

            # find label
            label = t.find("name").find("text").text

            transitions[identifier] = {"label": label}

        # parsing arcs
        arcs = {}
        for a in pnml.find_all("arc"):
            # get id
            identifier = a["id"]

            # find source
            source = a["source"]

            # find target
            target = a["target"]

            arcs[identifier] = {"source": source, "target": target}

        return {"places": places, "transitions": transitions, "arcs": arcs}


def parse_to_mista(name, input_file, output_file):
    """Parse a PNML file into a MISTA file

    Parameters
    ----------
    name : str
        model's name
    input_file : str
        path to the file to be parsed
    output_file : str
        path to the output file
    """
    model = parse_pnml_file(input_file)
    soup = BeautifulSoup(features="xml")

    # create pnml tag
    pnml = soup.new_tag("pnml")

    # create net tag
    net = soup.new_tag("net", id=name, type="PrT net")

    # add tokenclass tag
    net.append(
        BeautifulSoup(
            '<tokenclass id="Default" enabled="true" red="0" green="0" blue="0" />',
            "xml",
        )
    )

    # add initial marking tag
    assert all(
        p["initialMarking"] <= 1 for p in model["places"].values()
    ), "marking > 1 not supported"

    initialMarkings = ", ".join(
        [f'{p["label"]}' for p in model["places"].values() if p["initialMarking"] > 0]
    )
    labels = BeautifulSoup(
        f'<labels x="0" y="0" width="100" height="15" border="true"><text>INIT {initialMarkings}</text></labels>',
        "xml",
    )
    net.append(labels)

    # graphics tags
    graphics_str = '<graphics><position x="0" y="0"/></graphics>'
    name_str = (
        '<name><value>{value}</value><graphics><offset x="0" y="0"/></graphics></name>'
    )
    marking_str = '<initialMarking><value>Default,</value><graphics><offset x="0" y="0"/></initialMarking>'

    # add places
    for _, p in model["places"].items():
        place = soup.new_tag("place", id=p["label"])
        place.append(BeautifulSoup(graphics_str, "xml"))
        place.append(BeautifulSoup(name_str.format(value=p["label"]), "xml"))
        place.append(BeautifulSoup(marking_str, "xml"))
        place.append(BeautifulSoup("<capacity><value>0</value></capacity>", "xml"))
        net.append(place)

    # add transitions
    for _, t in model["transitions"].items():
        transition = soup.new_tag("transition", id=t["label"])
        transition.append(BeautifulSoup(graphics_str, "xml"))
        transition.append(BeautifulSoup(name_str.format(value=t["label"]), "xml"))
        transition.append(
            BeautifulSoup("<orientation><value>0</value></orientation>", "xml")
        )
        transition.append(BeautifulSoup("<guard><value/></guard>", "xml"))
        transition.append(BeautifulSoup("<effect><value/></effect>", "xml"))
        transition.append(BeautifulSoup("<subnet><value/></subnet>", "xml"))
        transition.append(BeautifulSoup("<rate><value>1.0</value></rate>", "xml"))
        transition.append(BeautifulSoup("<timed><value>true</value></timed>", "xml"))
        transition.append(
            BeautifulSoup(
                "<infiniteServer><value>false</value></infiniteServer>", "xml"
            )
        )
        transition.append(BeautifulSoup("<priority><value>1</value></priority>", "xml"))
        net.append(transition)

    # add arcs
    arcpath_str = '<arcpath id="{id_arc}" x="0" y="0" curvePoint="false" />'
    for _, a in model["arcs"].items():
        source = (
            model["places"][a["source"]]["label"]
            if a["source"] in model["places"]
            else model["transitions"][a["source"]]["label"]
        )
        target = (
            model["places"][a["target"]]["label"]
            if a["target"] in model["places"]
            else model["transitions"][a["target"]]["label"]
        )
        arc = soup.new_tag(
            "arc", id=f"{source} to {target}", source=source, target=target
        )
        arc.append(soup.new_tag("graphics"))
        arc.append(
            BeautifulSoup(
                "<inscription><value>Default,</value><graphics /></inscription>", "xml"
            )
        )
        arc.append(BeautifulSoup("<tagged><value>false</value></tagged>", "xml"))
        arc.append(BeautifulSoup(arcpath_str.format(id_arc="000"), "xml"))
        arc.append(BeautifulSoup(arcpath_str.format(id_arc="001"), "xml"))
        arc.append(BeautifulSoup('<type value="normal" />', "xml"))
        net.append(arc)

    pnml.append(net)
    soup.append(pnml)

    with open(output_file, "w", encoding="utf-8") as f_output:
        f_output.write(str(soup))


def copy_xmid_file(model_name, output_file, xmid_filename="template.xmid"):
    """Copy the xmid file for the mista tool

    Parameters
    ----------
    model_name : str
        model's name
    output_file : str
        path to the output file
    xmid_filename : str, optional
        path to the xmid template used, by default "template.xmid"
    """
    rb = xlrd.open_workbook(xmid_filename)
    wb = xlcopy(rb)
    w_sheet = wb.get_sheet(0)
    w_sheet.write(5, 2, model_name)
    wb.save(output_file)


def parse_to_sog(input_file, output_file):
    """Parse PNML files into SOG input format

    Parameters
    ----------
    input_file : str
        path to the input PNML file
    output_file : str
        path to the output .net file
    """
    model = parse_pnml_file(input_file)

    with open(output_file, "w") as f:
        places = model["places"]
        transitions = model["transitions"]

        # parse places
        for p in places.values():
            marking = p["initialMarking"]
            mk_str = f" mk({marking}<..>)" if marking > 0 else ""
            f.write(f'#place {p["label"]}{mk_str}\n')

        # list of arcs
        arcs = model["arcs"].values()

        # parse transitions
        for t_id, t in transitions.items():
            ingoing_arcs = "".join(
                [
                    f'{places[a["source"]]["label"]}:<..>;'
                    for a in arcs
                    if a["target"] == t_id
                ]
            )

            outgoing_arcs = "".join(
                [
                    f'{places[a["target"]]["label"]}:<..>;'
                    for a in arcs
                    if a["source"] == t_id
                ]
            )

            f.write(f'#trans {t["label"]}\n')
            f.write(f"in {{{ingoing_arcs}}}\n")
            f.write(f"out {{{outgoing_arcs}}}\n")
            f.write("#endtr\n")


def parse_to_nmodel(name, input_file, output_file):
    """Parse a DOT file into a NModel file

    Parameters
    ----------
    name : str
        model's name
    input_file : str
        path to the file to be parsed
    output_file : str
        path to the output file
    """
    template = """
    /*************************************************************************
    Transition {t}
    ***************************************************************************/
    public static bool {t}Enabled() {{
        return {pre};
    }}
    [Action]
    public static void {t}(){{
        switch (state) {{
            {post}
        }}
    }}
    """
    with open(output_file, "w") as f:
        className = name.title().replace("-", "_")
        graph = parse_dot_file(input_file)
        states = graph["states"]
        arcs = graph["arcs"]
        init_state = graph["initial_state"]

        # write includes
        includes = [
            "using NModel;",
            "using NModel.Attributes;",
            "using NModel.Execution;",
            f"namespace {className} {{\n",
            f"public static class {className} {{\n",
        ]
        f.write("\n".join(includes))

        # write states
        states_dict = {v: f"_{i}" for i, v in enumerate(states)}
        states_str = ", ".join([i for _, i in states_dict.items()])
        state_enum = f"public enum State {{ {states_str} }};\n"
        f.write(state_enum)

        # write initial state
        f.write(f"public static State state = State.{states_dict[init_state]};")

        # write transitions
        transitions = defaultdict(list)
        [transitions[l].append([s, t]) for (s, t, l) in arcs]

        for t_label, v in transitions.items():
            pre = [f"(state == State.{states_dict[s[0]]})" for s in v]
            pre_str = " || ".join(pre)

            post = [
                "\n                ".join(
                    [
                        f"case State.{states_dict[s[0]]}:",
                        f"state = State.{states_dict[s[1]]};",
                        "break;",
                    ]
                )
                for s in v
            ]
            post_str = "\n            ".join(post)
            f.write(template.format(t=t_label, pre=pre_str, post=post_str))

        factory = [
            "public static class Factory {",
            "public static ModelProgram Create() {",
            f'return new LibraryModelProgram(typeof(Factory).Assembly, "{className}");',
            "}\n}\n}",
        ]
        f.write("}" + "\n".join(factory))


def main(tool, input_file):
    """Main function of the program

    Parameters
    ----------
    tool : str
        target tool
    input_file : str
        path to the input file for the tool
    """
    folder, filename = os.path.split(input_file)
    model, extension = filename.split(".")

    if tool == "mista":
        if extension != "pnml":
            raise RuntimeError("Only PNML is supported")
        mista_filename = filename.replace(f".{extension}", ".xml")
        output_mista_file = os.path.join(folder, mista_filename)
        print(f"generating MISTA model: {output_mista_file}")
        parse_to_mista(model, input_file, output_mista_file)

        # copy xmid file for the model
        xmid_file = os.path.join(folder, mista_filename.replace(".xml", ".xmid"))
        copy_xmid_file(mista_filename, xmid_file)
    elif tool == "sog":
        if extension != "pnml":
            raise RuntimeError("Only PNML is supported")
        sog_filename = filename.replace(f".{extension}", ".net")
        output_sog_file = os.path.join(folder, sog_filename)
        print(f"generating SOG model: {output_sog_file}")
        parse_to_sog(input_file, output_sog_file)
    elif tool == "nmodel":
        if extension != "dot":
            raise RuntimeError("Only DOT is supported")
        nmodel_filename = filename.replace(f".{extension}", ".cs")
        output_nmodel_file = os.path.join(folder, nmodel_filename)
        print(f"generating NModel model: {output_nmodel_file}")
        parse_to_nmodel(model, input_file, output_nmodel_file)
    else:
        raise RuntimeError(f"Tool {tool} is not supported")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Parser from several formats""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument("--input", type=str, required=True, help="input file")
    parser.add_argument(
        "--tool",
        choices=["mista", "nmodel", "sog"],
        required=True,
        help="output format",
    )

    args = parser.parse_args()
    main(args.tool, args.input)
