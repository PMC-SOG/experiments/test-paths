using NModel;
using NModel.Attributes;
using NModel.Execution;
namespace Example {

public static class Example {
public enum State { _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14 };
public static State state = State._4;
    /*************************************************************************
    Transition t1
    ***************************************************************************/
    public static bool t1Enabled() {
        return (state == State._4);
    }
    [Action]
    public static void t1(){
        switch (state) {
            case State._4:
                state = State._6;
                break;
        }
    }
    
    /*************************************************************************
    Transition t2
    ***************************************************************************/
    public static bool t2Enabled() {
        return (state == State._6);
    }
    [Action]
    public static void t2(){
        switch (state) {
            case State._6:
                state = State._7;
                break;
        }
    }
    
    /*************************************************************************
    Transition t3
    ***************************************************************************/
    public static bool t3Enabled() {
        return (state == State._7) || (state == State._8) || (state == State._9) || (state == State._3);
    }
    [Action]
    public static void t3(){
        switch (state) {
            case State._7:
                state = State._5;
                break;
            case State._8:
                state = State._11;
                break;
            case State._9:
                state = State._12;
                break;
            case State._3:
                state = State._14;
                break;
        }
    }
    
    /*************************************************************************
    Transition t5
    ***************************************************************************/
    public static bool t5Enabled() {
        return (state == State._7) || (state == State._5) || (state == State._1);
    }
    [Action]
    public static void t5(){
        switch (state) {
            case State._7:
                state = State._8;
                break;
            case State._5:
                state = State._11;
                break;
            case State._1:
                state = State._10;
                break;
        }
    }
    
    /*************************************************************************
    Transition t6
    ***************************************************************************/
    public static bool t6Enabled() {
        return (state == State._7) || (state == State._5) || (state == State._1);
    }
    [Action]
    public static void t6(){
        switch (state) {
            case State._7:
                state = State._9;
                break;
            case State._5:
                state = State._12;
                break;
            case State._1:
                state = State._0;
                break;
        }
    }
    
    /*************************************************************************
    Transition t4
    ***************************************************************************/
    public static bool t4Enabled() {
        return (state == State._5) || (state == State._11) || (state == State._12) || (state == State._14);
    }
    [Action]
    public static void t4(){
        switch (state) {
            case State._5:
                state = State._1;
                break;
            case State._11:
                state = State._10;
                break;
            case State._12:
                state = State._0;
                break;
            case State._14:
                state = State._13;
                break;
        }
    }
    
    /*************************************************************************
    Transition t7
    ***************************************************************************/
    public static bool t7Enabled() {
        return (state == State._8) || (state == State._11) || (state == State._10);
    }
    [Action]
    public static void t7(){
        switch (state) {
            case State._8:
                state = State._3;
                break;
            case State._11:
                state = State._14;
                break;
            case State._10:
                state = State._13;
                break;
        }
    }
    
    /*************************************************************************
    Transition t8
    ***************************************************************************/
    public static bool t8Enabled() {
        return (state == State._9) || (state == State._12) || (state == State._0);
    }
    [Action]
    public static void t8(){
        switch (state) {
            case State._9:
                state = State._3;
                break;
            case State._12:
                state = State._14;
                break;
            case State._0:
                state = State._13;
                break;
        }
    }
    
    /*************************************************************************
    Transition t10
    ***************************************************************************/
    public static bool t10Enabled() {
        return (state == State._3) || (state == State._14) || (state == State._13);
    }
    [Action]
    public static void t10(){
        switch (state) {
            case State._3:
                state = State._7;
                break;
            case State._14:
                state = State._5;
                break;
            case State._13:
                state = State._1;
                break;
        }
    }
    
    /*************************************************************************
    Transition t9
    ***************************************************************************/
    public static bool t9Enabled() {
        return (state == State._13);
    }
    [Action]
    public static void t9(){
        switch (state) {
            case State._13:
                state = State._2;
                break;
        }
    }
    
    /*************************************************************************
    Transition t11
    ***************************************************************************/
    public static bool t11Enabled() {
        return (state == State._2);
    }
    [Action]
    public static void t11(){
        switch (state) {
            case State._2:
                state = State._4;
                break;
        }
    }
    }public static class Factory {
public static ModelProgram Create() {
return new LibraryModelProgram(typeof(Factory).Assembly, "Example");
}
}
}