# Symbolic Observation Graph-Based Generation of Test Paths

This folder contains the scripts to generate the benchmarks of the paper.

## Clone this repository

```
git clone https://depot.lipn.univ-paris13.fr/PMC-SOG/experiments/test-paths.git && cd test-paths
```

## Folder Structure

```
.
├── analysis.ipynb    # Python notebook to generate the plots (analysis.py)
├── analysis.py
├── benchmarks.sh     # Bash script to run the benchmarks
├── images            # Folder containing the plots of the benchmark results
├── models            # Folder containing the models of the benchmark
├── requirements.txt  # File containing the python requirements
├── results.csv       # File containing the results of the benchmark
├── scripts           # Folder containing scripts to parser models into formats accepted by the tools
└── tools             # Folder containing the tools compared in the benchmarks
```

## Run benchmarks

The script `benchmarks.sh` allows to run the benchmarks for a specific model.
The keyword `all` can be used instead of a specific model in order to run the
benchmarks for all the models. The argument `-b` corresponds to the bound of the
SOG.

```
λ> ./benchmarks.sh
Usage: ./benchmarks.sh -m MODEL -b BOUND -t TIMEOUT
```
