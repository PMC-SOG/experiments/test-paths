using NModel;
using NModel.Attributes;
using NModel.Execution;
namespace Sudokua_1 {

public static class Sudokua_1 {
public enum State { _0, _1 };
public static State state = State._0;
    /*************************************************************************
    Transition select_0_0_0
    ***************************************************************************/
    public static bool select_0_0_0Enabled() {
        return (state == State._0);
    }
    [Action]
    public static void select_0_0_0(){
        switch (state) {
            case State._0:
                state = State._1;
                break;
        }
    }
    }public static class Factory {
public static ModelProgram Create() {
return new LibraryModelProgram(typeof(Factory).Assembly, "Sudokua_1");
}
}
}