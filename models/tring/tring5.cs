using NModel;
using NModel.Attributes;
using NModel.Execution;
namespace Tring5 {

public static class Tring5 {
public enum State { _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _70, _71, _72, _73, _74, _75, _76, _77, _78, _79, _80, _81, _82, _83, _84, _85, _86, _87, _88, _89, _90, _91, _92, _93, _94, _95, _96, _97, _98, _99, _100, _101, _102, _103, _104, _105, _106, _107, _108, _109, _110, _111, _112, _113, _114, _115, _116, _117, _118, _119, _120, _121, _122, _123, _124, _125, _126, _127, _128, _129, _130, _131, _132, _133, _134, _135, _136, _137, _138, _139, _140, _141, _142, _143, _144, _145, _146, _147, _148, _149, _150, _151, _152, _153, _154, _155, _156, _157, _158, _159, _160, _161, _162, _163, _164, _165 };
public static State state = State._162;
    /*************************************************************************
    Transition OtherProcess_1_1_0
    ***************************************************************************/
    public static bool OtherProcess_1_1_0Enabled() {
        return (state == State._162) || (state == State._132) || (state == State._88) || (state == State._153) || (state == State._45) || (state == State._62) || (state == State._142) || (state == State._106) || (state == State._75) || (state == State._123) || (state == State._42) || (state == State._131) || (state == State._135) || (state == State._113) || (state == State._31) || (state == State._37) || (state == State._5) || (state == State._98) || (state == State._50) || (state == State._71) || (state == State._91) || (state == State._120) || (state == State._23) || (state == State._85) || (state == State._164) || (state == State._28) || (state == State._56) || (state == State._48) || (state == State._149) || (state == State._15) || (state == State._29) || (state == State._107) || (state == State._150) || (state == State._161) || (state == State._117) || (state == State._165) || (state == State._139) || (state == State._68) || (state == State._81) || (state == State._159) || (state == State._137) || (state == State._101);
    }
    [Action]
    public static void OtherProcess_1_1_0(){
        switch (state) {
            case State._162:
                state = State._8;
                break;
            case State._132:
                state = State._39;
                break;
            case State._88:
                state = State._108;
                break;
            case State._153:
                state = State._22;
                break;
            case State._45:
                state = State._140;
                break;
            case State._62:
                state = State._112;
                break;
            case State._142:
                state = State._80;
                break;
            case State._106:
                state = State._66;
                break;
            case State._75:
                state = State._17;
                break;
            case State._123:
                state = State._9;
                break;
            case State._42:
                state = State._51;
                break;
            case State._131:
                state = State._4;
                break;
            case State._135:
                state = State._96;
                break;
            case State._113:
                state = State._2;
                break;
            case State._31:
                state = State._89;
                break;
            case State._37:
                state = State._52;
                break;
            case State._5:
                state = State._102;
                break;
            case State._98:
                state = State._148;
                break;
            case State._50:
                state = State._35;
                break;
            case State._71:
                state = State._154;
                break;
            case State._91:
                state = State._24;
                break;
            case State._120:
                state = State._13;
                break;
            case State._23:
                state = State._95;
                break;
            case State._85:
                state = State._41;
                break;
            case State._164:
                state = State._72;
                break;
            case State._28:
                state = State._92;
                break;
            case State._56:
                state = State._156;
                break;
            case State._48:
                state = State._86;
                break;
            case State._149:
                state = State._100;
                break;
            case State._15:
                state = State._144;
                break;
            case State._29:
                state = State._109;
                break;
            case State._107:
                state = State._157;
                break;
            case State._150:
                state = State._40;
                break;
            case State._161:
                state = State._12;
                break;
            case State._117:
                state = State._77;
                break;
            case State._165:
                state = State._55;
                break;
            case State._139:
                state = State._127;
                break;
            case State._68:
                state = State._146;
                break;
            case State._81:
                state = State._138;
                break;
            case State._159:
                state = State._97;
                break;
            case State._137:
                state = State._36;
                break;
            case State._101:
                state = State._49;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_5_4
    ***************************************************************************/
    public static bool OtherProcess_5_5_4Enabled() {
        return (state == State._162) || (state == State._8) || (state == State._88) || (state == State._153) || (state == State._34) || (state == State._108) || (state == State._75) || (state == State._22) || (state == State._42) || (state == State._145) || (state == State._130) || (state == State._17) || (state == State._51) || (state == State._126);
    }
    [Action]
    public static void OtherProcess_5_5_4(){
        switch (state) {
            case State._162:
                state = State._132;
                break;
            case State._8:
                state = State._39;
                break;
            case State._88:
                state = State._62;
                break;
            case State._153:
                state = State._142;
                break;
            case State._34:
                state = State._147;
                break;
            case State._108:
                state = State._112;
                break;
            case State._75:
                state = State._31;
                break;
            case State._22:
                state = State._80;
                break;
            case State._42:
                state = State._5;
                break;
            case State._145:
                state = State._74;
                break;
            case State._130:
                state = State._125;
                break;
            case State._17:
                state = State._89;
                break;
            case State._51:
                state = State._102;
                break;
            case State._126:
                state = State._30;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_3_2
    ***************************************************************************/
    public static bool OtherProcess_3_3_2Enabled() {
        return (state == State._162) || (state == State._8) || (state == State._132) || (state == State._45) || (state == State._39) || (state == State._140) || (state == State._106) || (state == State._135) || (state == State._66) || (state == State._96);
    }
    [Action]
    public static void OtherProcess_3_3_2(){
        switch (state) {
            case State._162:
                state = State._88;
                break;
            case State._8:
                state = State._108;
                break;
            case State._132:
                state = State._62;
                break;
            case State._45:
                state = State._113;
                break;
            case State._39:
                state = State._112;
                break;
            case State._140:
                state = State._2;
                break;
            case State._106:
                state = State._50;
                break;
            case State._135:
                state = State._164;
                break;
            case State._66:
                state = State._35;
                break;
            case State._96:
                state = State._72;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_2_1
    ***************************************************************************/
    public static bool OtherProcess_2_2_1Enabled() {
        return (state == State._162) || (state == State._132) || (state == State._88) || (state == State._45) || (state == State._62) || (state == State._106) || (state == State._123) || (state == State._135) || (state == State._113) || (state == State._37) || (state == State._50) || (state == State._91) || (state == State._164) || (state == State._117);
    }
    [Action]
    public static void OtherProcess_2_2_1(){
        switch (state) {
            case State._162:
                state = State._153;
                break;
            case State._132:
                state = State._142;
                break;
            case State._88:
                state = State._75;
                break;
            case State._45:
                state = State._131;
                break;
            case State._62:
                state = State._31;
                break;
            case State._106:
                state = State._98;
                break;
            case State._123:
                state = State._71;
                break;
            case State._135:
                state = State._85;
                break;
            case State._113:
                state = State._28;
                break;
            case State._37:
                state = State._56;
                break;
            case State._50:
                state = State._15;
                break;
            case State._91:
                state = State._29;
                break;
            case State._164:
                state = State._165;
                break;
            case State._117:
                state = State._159;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_4_3
    ***************************************************************************/
    public static bool OtherProcess_4_4_3Enabled() {
        return (state == State._162) || (state == State._8) || (state == State._132) || (state == State._153) || (state == State._34) || (state == State._39) || (state == State._142) || (state == State._22) || (state == State._147) || (state == State._80);
    }
    [Action]
    public static void OtherProcess_4_4_3(){
        switch (state) {
            case State._162:
                state = State._45;
                break;
            case State._8:
                state = State._140;
                break;
            case State._132:
                state = State._106;
                break;
            case State._153:
                state = State._131;
                break;
            case State._34:
                state = State._82;
                break;
            case State._39:
                state = State._66;
                break;
            case State._142:
                state = State._98;
                break;
            case State._22:
                state = State._4;
                break;
            case State._147:
                state = State._110;
                break;
            case State._80:
                state = State._148;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_2_0
    ***************************************************************************/
    public static bool OtherProcess_2_2_0Enabled() {
        return (state == State._8) || (state == State._39) || (state == State._108) || (state == State._140) || (state == State._112) || (state == State._66) || (state == State._9) || (state == State._96) || (state == State._2) || (state == State._52) || (state == State._35) || (state == State._24) || (state == State._72) || (state == State._77);
    }
    [Action]
    public static void OtherProcess_2_2_0(){
        switch (state) {
            case State._8:
                state = State._34;
                break;
            case State._39:
                state = State._147;
                break;
            case State._108:
                state = State._130;
                break;
            case State._140:
                state = State._82;
                break;
            case State._112:
                state = State._125;
                break;
            case State._66:
                state = State._110;
                break;
            case State._9:
                state = State._54;
                break;
            case State._96:
                state = State._38;
                break;
            case State._2:
                state = State._79;
                break;
            case State._52:
                state = State._158;
                break;
            case State._35:
                state = State._155;
                break;
            case State._24:
                state = State._16;
                break;
            case State._72:
                state = State._18;
                break;
            case State._77:
                state = State._57;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_4_2
    ***************************************************************************/
    public static bool OtherProcess_4_4_2Enabled() {
        return (state == State._88) || (state == State._108) || (state == State._62) || (state == State._75) || (state == State._112) || (state == State._130) || (state == State._31) || (state == State._17) || (state == State._125) || (state == State._89);
    }
    [Action]
    public static void OtherProcess_4_4_2(){
        switch (state) {
            case State._88:
                state = State._123;
                break;
            case State._108:
                state = State._9;
                break;
            case State._62:
                state = State._37;
                break;
            case State._75:
                state = State._71;
                break;
            case State._112:
                state = State._52;
                break;
            case State._130:
                state = State._54;
                break;
            case State._31:
                state = State._56;
                break;
            case State._17:
                state = State._154;
                break;
            case State._125:
                state = State._158;
                break;
            case State._89:
                state = State._156;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_3_1
    ***************************************************************************/
    public static bool OtherProcess_3_3_1Enabled() {
        return (state == State._153) || (state == State._142) || (state == State._22) || (state == State._131) || (state == State._80) || (state == State._98) || (state == State._4) || (state == State._85) || (state == State._148) || (state == State._41);
    }
    [Action]
    public static void OtherProcess_3_3_1(){
        switch (state) {
            case State._153:
                state = State._42;
                break;
            case State._142:
                state = State._5;
                break;
            case State._22:
                state = State._51;
                break;
            case State._131:
                state = State._23;
                break;
            case State._80:
                state = State._102;
                break;
            case State._98:
                state = State._149;
                break;
            case State._4:
                state = State._95;
                break;
            case State._85:
                state = State._161;
                break;
            case State._148:
                state = State._100;
                break;
            case State._41:
                state = State._12;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_5_3
    ***************************************************************************/
    public static bool OtherProcess_5_5_3Enabled() {
        return (state == State._45) || (state == State._140) || (state == State._131) || (state == State._113) || (state == State._82) || (state == State._2) || (state == State._4) || (state == State._23) || (state == State._28) || (state == State._163) || (state == State._79) || (state == State._95) || (state == State._92) || (state == State._61);
    }
    [Action]
    public static void OtherProcess_5_5_3(){
        switch (state) {
            case State._45:
                state = State._135;
                break;
            case State._140:
                state = State._96;
                break;
            case State._131:
                state = State._85;
                break;
            case State._113:
                state = State._164;
                break;
            case State._82:
                state = State._38;
                break;
            case State._2:
                state = State._72;
                break;
            case State._4:
                state = State._41;
                break;
            case State._23:
                state = State._161;
                break;
            case State._28:
                state = State._165;
                break;
            case State._163:
                state = State._116;
                break;
            case State._79:
                state = State._18;
                break;
            case State._95:
                state = State._12;
                break;
            case State._92:
                state = State._55;
                break;
            case State._61:
                state = State._115;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_3_0
    ***************************************************************************/
    public static bool OtherProcess_3_3_0Enabled() {
        return (state == State._34) || (state == State._147) || (state == State._82) || (state == State._110) || (state == State._38);
    }
    [Action]
    public static void OtherProcess_3_3_0(){
        switch (state) {
            case State._34:
                state = State._145;
                break;
            case State._147:
                state = State._74;
                break;
            case State._82:
                state = State._163;
                break;
            case State._110:
                state = State._121;
                break;
            case State._38:
                state = State._116;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_4_3
    ***************************************************************************/
    public static bool OtherProcess_5_4_3Enabled() {
        return (state == State._106) || (state == State._66) || (state == State._98) || (state == State._50) || (state == State._110) || (state == State._35) || (state == State._148) || (state == State._149) || (state == State._15) || (state == State._121) || (state == State._155) || (state == State._100) || (state == State._144) || (state == State._76);
    }
    [Action]
    public static void OtherProcess_5_4_3(){
        switch (state) {
            case State._106:
                state = State._135;
                break;
            case State._66:
                state = State._96;
                break;
            case State._98:
                state = State._85;
                break;
            case State._50:
                state = State._164;
                break;
            case State._110:
                state = State._38;
                break;
            case State._35:
                state = State._72;
                break;
            case State._148:
                state = State._41;
                break;
            case State._149:
                state = State._161;
                break;
            case State._15:
                state = State._165;
                break;
            case State._121:
                state = State._116;
                break;
            case State._155:
                state = State._18;
                break;
            case State._100:
                state = State._12;
                break;
            case State._144:
                state = State._55;
                break;
            case State._76:
                state = State._115;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_2_1
    ***************************************************************************/
    public static bool OtherProcess_3_2_1Enabled() {
        return (state == State._75) || (state == State._31) || (state == State._17) || (state == State._71) || (state == State._28) || (state == State._89) || (state == State._56) || (state == State._15) || (state == State._154) || (state == State._29) || (state == State._165) || (state == State._92) || (state == State._156) || (state == State._144) || (state == State._109) || (state == State._159) || (state == State._55) || (state == State._97);
    }
    [Action]
    public static void OtherProcess_3_2_1(){
        switch (state) {
            case State._75:
                state = State._42;
                break;
            case State._31:
                state = State._5;
                break;
            case State._17:
                state = State._51;
                break;
            case State._71:
                state = State._107;
                break;
            case State._28:
                state = State._23;
                break;
            case State._89:
                state = State._102;
                break;
            case State._56:
                state = State._139;
                break;
            case State._15:
                state = State._149;
                break;
            case State._154:
                state = State._157;
                break;
            case State._29:
                state = State._68;
                break;
            case State._165:
                state = State._161;
                break;
            case State._92:
                state = State._95;
                break;
            case State._156:
                state = State._127;
                break;
            case State._144:
                state = State._100;
                break;
            case State._109:
                state = State._146;
                break;
            case State._159:
                state = State._101;
                break;
            case State._55:
                state = State._12;
                break;
            case State._97:
                state = State._49;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_5_2
    ***************************************************************************/
    public static bool OtherProcess_5_5_2Enabled() {
        return (state == State._123) || (state == State._9) || (state == State._71) || (state == State._54) || (state == State._154) || (state == State._107) || (state == State._67) || (state == State._157) || (state == State._105);
    }
    [Action]
    public static void OtherProcess_5_5_2(){
        switch (state) {
            case State._123:
                state = State._91;
                break;
            case State._9:
                state = State._24;
                break;
            case State._71:
                state = State._29;
                break;
            case State._54:
                state = State._16;
                break;
            case State._154:
                state = State._109;
                break;
            case State._107:
                state = State._68;
                break;
            case State._67:
                state = State._3;
                break;
            case State._157:
                state = State._146;
                break;
            case State._105:
                state = State._65;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_1_0
    ***************************************************************************/
    public static bool OtherProcess_2_1_0Enabled() {
        return (state == State._22) || (state == State._80) || (state == State._17) || (state == State._51) || (state == State._4) || (state == State._89) || (state == State._102) || (state == State._148) || (state == State._154) || (state == State._13) || (state == State._95) || (state == State._41) || (state == State._92) || (state == State._156) || (state == State._86) || (state == State._100) || (state == State._144) || (state == State._109) || (state == State._157) || (state == State._40) || (state == State._12) || (state == State._55) || (state == State._127) || (state == State._146) || (state == State._138) || (state == State._97) || (state == State._36) || (state == State._49);
    }
    [Action]
    public static void OtherProcess_2_1_0(){
        switch (state) {
            case State._22:
                state = State._34;
                break;
            case State._80:
                state = State._147;
                break;
            case State._17:
                state = State._130;
                break;
            case State._51:
                state = State._126;
                break;
            case State._4:
                state = State._82;
                break;
            case State._89:
                state = State._125;
                break;
            case State._102:
                state = State._30;
                break;
            case State._148:
                state = State._110;
                break;
            case State._154:
                state = State._54;
                break;
            case State._13:
                state = State._11;
                break;
            case State._95:
                state = State._61;
                break;
            case State._41:
                state = State._38;
                break;
            case State._92:
                state = State._79;
                break;
            case State._156:
                state = State._158;
                break;
            case State._86:
                state = State._26;
                break;
            case State._100:
                state = State._76;
                break;
            case State._144:
                state = State._155;
                break;
            case State._109:
                state = State._16;
                break;
            case State._157:
                state = State._105;
                break;
            case State._40:
                state = State._111;
                break;
            case State._12:
                state = State._115;
                break;
            case State._55:
                state = State._18;
                break;
            case State._127:
                state = State._118;
                break;
            case State._146:
                state = State._65;
                break;
            case State._138:
                state = State._114;
                break;
            case State._97:
                state = State._57;
                break;
            case State._36:
                state = State._141;
                break;
            case State._49:
                state = State._133;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_4_1
    ***************************************************************************/
    public static bool OtherProcess_4_4_1Enabled() {
        return (state == State._42) || (state == State._5) || (state == State._51) || (state == State._102) || (state == State._126) || (state == State._30);
    }
    [Action]
    public static void OtherProcess_4_4_1(){
        switch (state) {
            case State._42:
                state = State._120;
                break;
            case State._5:
                state = State._48;
                break;
            case State._51:
                state = State._13;
                break;
            case State._102:
                state = State._86;
                break;
            case State._126:
                state = State._11;
                break;
            case State._30:
                state = State._26;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_3_2
    ***************************************************************************/
    public static bool OtherProcess_4_3_2Enabled() {
        return (state == State._113) || (state == State._2) || (state == State._50) || (state == State._164) || (state == State._28) || (state == State._35) || (state == State._72) || (state == State._79) || (state == State._15) || (state == State._165) || (state == State._92) || (state == State._155) || (state == State._18) || (state == State._144) || (state == State._55);
    }
    [Action]
    public static void OtherProcess_4_3_2(){
        switch (state) {
            case State._113:
                state = State._123;
                break;
            case State._2:
                state = State._9;
                break;
            case State._50:
                state = State._37;
                break;
            case State._164:
                state = State._117;
                break;
            case State._28:
                state = State._71;
                break;
            case State._35:
                state = State._52;
                break;
            case State._72:
                state = State._77;
                break;
            case State._79:
                state = State._54;
                break;
            case State._15:
                state = State._56;
                break;
            case State._165:
                state = State._159;
                break;
            case State._92:
                state = State._154;
                break;
            case State._155:
                state = State._158;
                break;
            case State._18:
                state = State._57;
                break;
            case State._144:
                state = State._156;
                break;
            case State._55:
                state = State._97;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_4_0
    ***************************************************************************/
    public static bool OtherProcess_4_4_0Enabled() {
        return (state == State._145) || (state == State._74);
    }
    [Action]
    public static void OtherProcess_4_4_0(){
        switch (state) {
            case State._145:
                state = State._32;
                break;
            case State._74:
                state = State._1;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_2_0
    ***************************************************************************/
    public static bool OtherProcess_3_2_0Enabled() {
        return (state == State._130) || (state == State._125) || (state == State._54) || (state == State._79) || (state == State._158) || (state == State._155) || (state == State._16) || (state == State._18) || (state == State._57);
    }
    [Action]
    public static void OtherProcess_3_2_0(){
        switch (state) {
            case State._130:
                state = State._145;
                break;
            case State._125:
                state = State._74;
                break;
            case State._54:
                state = State._67;
                break;
            case State._79:
                state = State._163;
                break;
            case State._158:
                state = State._90;
                break;
            case State._155:
                state = State._121;
                break;
            case State._16:
                state = State._3;
                break;
            case State._18:
                state = State._116;
                break;
            case State._57:
                state = State._0;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_4_2
    ***************************************************************************/
    public static bool OtherProcess_5_4_2Enabled() {
        return (state == State._37) || (state == State._52) || (state == State._56) || (state == State._158) || (state == State._156) || (state == State._139) || (state == State._90) || (state == State._127) || (state == State._118);
    }
    [Action]
    public static void OtherProcess_5_4_2(){
        switch (state) {
            case State._37:
                state = State._91;
                break;
            case State._52:
                state = State._24;
                break;
            case State._56:
                state = State._29;
                break;
            case State._158:
                state = State._16;
                break;
            case State._156:
                state = State._109;
                break;
            case State._139:
                state = State._68;
                break;
            case State._90:
                state = State._3;
                break;
            case State._127:
                state = State._146;
                break;
            case State._118:
                state = State._65;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_5_1
    ***************************************************************************/
    public static bool OtherProcess_5_5_1Enabled() {
        return (state == State._120) || (state == State._13) || (state == State._11) || (state == State._43);
    }
    [Action]
    public static void OtherProcess_5_5_1(){
        switch (state) {
            case State._120:
                state = State._150;
                break;
            case State._13:
                state = State._40;
                break;
            case State._11:
                state = State._111;
                break;
            case State._43:
                state = State._83;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_3_1
    ***************************************************************************/
    public static bool OtherProcess_4_3_1Enabled() {
        return (state == State._23) || (state == State._149) || (state == State._95) || (state == State._161) || (state == State._100) || (state == State._61) || (state == State._12) || (state == State._76) || (state == State._115);
    }
    [Action]
    public static void OtherProcess_4_3_1(){
        switch (state) {
            case State._23:
                state = State._120;
                break;
            case State._149:
                state = State._48;
                break;
            case State._95:
                state = State._13;
                break;
            case State._161:
                state = State._81;
                break;
            case State._100:
                state = State._86;
                break;
            case State._61:
                state = State._11;
                break;
            case State._12:
                state = State._138;
                break;
            case State._76:
                state = State._26;
                break;
            case State._115:
                state = State._114;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_5_0
    ***************************************************************************/
    public static bool OtherProcess_5_5_0Enabled() {
        return (state == State._32);
    }
    [Action]
    public static void OtherProcess_5_5_0(){
        switch (state) {
            case State._32:
                state = State._69;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_3_0
    ***************************************************************************/
    public static bool OtherProcess_4_3_0Enabled() {
        return (state == State._163) || (state == State._121) || (state == State._116);
    }
    [Action]
    public static void OtherProcess_4_3_0(){
        switch (state) {
            case State._163:
                state = State._32;
                break;
            case State._121:
                state = State._1;
                break;
            case State._116:
                state = State._122;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_4_1
    ***************************************************************************/
    public static bool OtherProcess_5_4_1Enabled() {
        return (state == State._48) || (state == State._86) || (state == State._26) || (state == State._14);
    }
    [Action]
    public static void OtherProcess_5_4_1(){
        switch (state) {
            case State._48:
                state = State._150;
                break;
            case State._86:
                state = State._40;
                break;
            case State._26:
                state = State._111;
                break;
            case State._14:
                state = State._83;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_2_1
    ***************************************************************************/
    public static bool OtherProcess_4_2_1Enabled() {
        return (state == State._107) || (state == State._139) || (state == State._157) || (state == State._68) || (state == State._127) || (state == State._146) || (state == State._105) || (state == State._101) || (state == State._118) || (state == State._65) || (state == State._49) || (state == State._133);
    }
    [Action]
    public static void OtherProcess_4_2_1(){
        switch (state) {
            case State._107:
                state = State._120;
                break;
            case State._139:
                state = State._48;
                break;
            case State._157:
                state = State._13;
                break;
            case State._68:
                state = State._137;
                break;
            case State._127:
                state = State._86;
                break;
            case State._146:
                state = State._36;
                break;
            case State._105:
                state = State._11;
                break;
            case State._101:
                state = State._81;
                break;
            case State._118:
                state = State._26;
                break;
            case State._65:
                state = State._141;
                break;
            case State._49:
                state = State._138;
                break;
            case State._133:
                state = State._114;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_1_0
    ***************************************************************************/
    public static bool OtherProcess_3_1_0Enabled() {
        return (state == State._126) || (state == State._30) || (state == State._11) || (state == State._61) || (state == State._26) || (state == State._76) || (state == State._105) || (state == State._111) || (state == State._115) || (state == State._118) || (state == State._65) || (state == State._114) || (state == State._141) || (state == State._133);
    }
    [Action]
    public static void OtherProcess_3_1_0(){
        switch (state) {
            case State._126:
                state = State._145;
                break;
            case State._30:
                state = State._74;
                break;
            case State._11:
                state = State._43;
                break;
            case State._61:
                state = State._163;
                break;
            case State._26:
                state = State._14;
                break;
            case State._76:
                state = State._121;
                break;
            case State._105:
                state = State._67;
                break;
            case State._111:
                state = State._83;
                break;
            case State._115:
                state = State._116;
                break;
            case State._118:
                state = State._90;
                break;
            case State._65:
                state = State._3;
                break;
            case State._114:
                state = State._53;
                break;
            case State._141:
                state = State._20;
                break;
            case State._133:
                state = State._0;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_3_2
    ***************************************************************************/
    public static bool OtherProcess_5_3_2Enabled() {
        return (state == State._117) || (state == State._77) || (state == State._159) || (state == State._57) || (state == State._97) || (state == State._101) || (state == State._0) || (state == State._49) || (state == State._133);
    }
    [Action]
    public static void OtherProcess_5_3_2(){
        switch (state) {
            case State._117:
                state = State._91;
                break;
            case State._77:
                state = State._24;
                break;
            case State._159:
                state = State._29;
                break;
            case State._57:
                state = State._16;
                break;
            case State._97:
                state = State._109;
                break;
            case State._101:
                state = State._68;
                break;
            case State._0:
                state = State._3;
                break;
            case State._49:
                state = State._146;
                break;
            case State._133:
                state = State._65;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_4_0
    ***************************************************************************/
    public static bool OtherProcess_5_4_0Enabled() {
        return (state == State._1);
    }
    [Action]
    public static void OtherProcess_5_4_0(){
        switch (state) {
            case State._1:
                state = State._69;
                break;
        }
    }
    
    /*************************************************************************
    Transition MainProcess_0
    ***************************************************************************/
    public static bool MainProcess_0Enabled() {
        return (state == State._69);
    }
    [Action]
    public static void MainProcess_0(){
        switch (state) {
            case State._69:
                state = State._64;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_2_0
    ***************************************************************************/
    public static bool OtherProcess_4_2_0Enabled() {
        return (state == State._67) || (state == State._90) || (state == State._3) || (state == State._0);
    }
    [Action]
    public static void OtherProcess_4_2_0(){
        switch (state) {
            case State._67:
                state = State._32;
                break;
            case State._90:
                state = State._1;
                break;
            case State._3:
                state = State._104;
                break;
            case State._0:
                state = State._122;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_3_1
    ***************************************************************************/
    public static bool OtherProcess_5_3_1Enabled() {
        return (state == State._81) || (state == State._138) || (state == State._114) || (state == State._53);
    }
    [Action]
    public static void OtherProcess_5_3_1(){
        switch (state) {
            case State._81:
                state = State._150;
                break;
            case State._138:
                state = State._40;
                break;
            case State._114:
                state = State._111;
                break;
            case State._53:
                state = State._83;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_1_0_1
    ***************************************************************************/
    public static bool OtherProcess_1_0_1Enabled() {
        return (state == State._64);
    }
    [Action]
    public static void OtherProcess_1_0_1(){
        switch (state) {
            case State._64:
                state = State._103;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_3_0
    ***************************************************************************/
    public static bool OtherProcess_5_3_0Enabled() {
        return (state == State._122);
    }
    [Action]
    public static void OtherProcess_5_3_0(){
        switch (state) {
            case State._122:
                state = State._69;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_2_1
    ***************************************************************************/
    public static bool OtherProcess_5_2_1Enabled() {
        return (state == State._137) || (state == State._36) || (state == State._141) || (state == State._20);
    }
    [Action]
    public static void OtherProcess_5_2_1(){
        switch (state) {
            case State._137:
                state = State._150;
                break;
            case State._36:
                state = State._40;
                break;
            case State._141:
                state = State._111;
                break;
            case State._20:
                state = State._83;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_1_0
    ***************************************************************************/
    public static bool OtherProcess_4_1_0Enabled() {
        return (state == State._43) || (state == State._14) || (state == State._83) || (state == State._53) || (state == State._20);
    }
    [Action]
    public static void OtherProcess_4_1_0(){
        switch (state) {
            case State._43:
                state = State._32;
                break;
            case State._14:
                state = State._1;
                break;
            case State._83:
                state = State._160;
                break;
            case State._53:
                state = State._122;
                break;
            case State._20:
                state = State._104;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_0_1
    ***************************************************************************/
    public static bool OtherProcess_2_0_1Enabled() {
        return (state == State._103);
    }
    [Action]
    public static void OtherProcess_2_0_1(){
        switch (state) {
            case State._103:
                state = State._128;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_2_0
    ***************************************************************************/
    public static bool OtherProcess_5_2_0Enabled() {
        return (state == State._104);
    }
    [Action]
    public static void OtherProcess_5_2_0(){
        switch (state) {
            case State._104:
                state = State._69;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_0_1
    ***************************************************************************/
    public static bool OtherProcess_3_0_1Enabled() {
        return (state == State._128);
    }
    [Action]
    public static void OtherProcess_3_0_1(){
        switch (state) {
            case State._128:
                state = State._19;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_1_0
    ***************************************************************************/
    public static bool OtherProcess_5_1_0Enabled() {
        return (state == State._160);
    }
    [Action]
    public static void OtherProcess_5_1_0(){
        switch (state) {
            case State._160:
                state = State._69;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_0_1
    ***************************************************************************/
    public static bool OtherProcess_4_0_1Enabled() {
        return (state == State._19);
    }
    [Action]
    public static void OtherProcess_4_0_1(){
        switch (state) {
            case State._19:
                state = State._87;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_0_1
    ***************************************************************************/
    public static bool OtherProcess_5_0_1Enabled() {
        return (state == State._87);
    }
    [Action]
    public static void OtherProcess_5_0_1(){
        switch (state) {
            case State._87:
                state = State._124;
                break;
        }
    }
    
    /*************************************************************************
    Transition MainProcess_1
    ***************************************************************************/
    public static bool MainProcess_1Enabled() {
        return (state == State._124);
    }
    [Action]
    public static void MainProcess_1(){
        switch (state) {
            case State._124:
                state = State._129;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_1_1_2
    ***************************************************************************/
    public static bool OtherProcess_1_1_2Enabled() {
        return (state == State._129);
    }
    [Action]
    public static void OtherProcess_1_1_2(){
        switch (state) {
            case State._129:
                state = State._151;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_1_2
    ***************************************************************************/
    public static bool OtherProcess_2_1_2Enabled() {
        return (state == State._151);
    }
    [Action]
    public static void OtherProcess_2_1_2(){
        switch (state) {
            case State._151:
                state = State._84;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_1_2
    ***************************************************************************/
    public static bool OtherProcess_3_1_2Enabled() {
        return (state == State._84);
    }
    [Action]
    public static void OtherProcess_3_1_2(){
        switch (state) {
            case State._84:
                state = State._10;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_1_2
    ***************************************************************************/
    public static bool OtherProcess_4_1_2Enabled() {
        return (state == State._10);
    }
    [Action]
    public static void OtherProcess_4_1_2(){
        switch (state) {
            case State._10:
                state = State._136;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_1_2
    ***************************************************************************/
    public static bool OtherProcess_5_1_2Enabled() {
        return (state == State._136);
    }
    [Action]
    public static void OtherProcess_5_1_2(){
        switch (state) {
            case State._136:
                state = State._27;
                break;
        }
    }
    
    /*************************************************************************
    Transition MainProcess_2
    ***************************************************************************/
    public static bool MainProcess_2Enabled() {
        return (state == State._27);
    }
    [Action]
    public static void MainProcess_2(){
        switch (state) {
            case State._27:
                state = State._94;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_1_2_3
    ***************************************************************************/
    public static bool OtherProcess_1_2_3Enabled() {
        return (state == State._94);
    }
    [Action]
    public static void OtherProcess_1_2_3(){
        switch (state) {
            case State._94:
                state = State._25;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_2_3
    ***************************************************************************/
    public static bool OtherProcess_2_2_3Enabled() {
        return (state == State._25);
    }
    [Action]
    public static void OtherProcess_2_2_3(){
        switch (state) {
            case State._25:
                state = State._119;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_2_3
    ***************************************************************************/
    public static bool OtherProcess_3_2_3Enabled() {
        return (state == State._119);
    }
    [Action]
    public static void OtherProcess_3_2_3(){
        switch (state) {
            case State._119:
                state = State._152;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_2_3
    ***************************************************************************/
    public static bool OtherProcess_4_2_3Enabled() {
        return (state == State._152);
    }
    [Action]
    public static void OtherProcess_4_2_3(){
        switch (state) {
            case State._152:
                state = State._99;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_2_3
    ***************************************************************************/
    public static bool OtherProcess_5_2_3Enabled() {
        return (state == State._99);
    }
    [Action]
    public static void OtherProcess_5_2_3(){
        switch (state) {
            case State._99:
                state = State._59;
                break;
        }
    }
    
    /*************************************************************************
    Transition MainProcess_3
    ***************************************************************************/
    public static bool MainProcess_3Enabled() {
        return (state == State._59);
    }
    [Action]
    public static void MainProcess_3(){
        switch (state) {
            case State._59:
                state = State._46;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_1_3_4
    ***************************************************************************/
    public static bool OtherProcess_1_3_4Enabled() {
        return (state == State._46);
    }
    [Action]
    public static void OtherProcess_1_3_4(){
        switch (state) {
            case State._46:
                state = State._33;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_3_4
    ***************************************************************************/
    public static bool OtherProcess_2_3_4Enabled() {
        return (state == State._33);
    }
    [Action]
    public static void OtherProcess_2_3_4(){
        switch (state) {
            case State._33:
                state = State._134;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_3_4
    ***************************************************************************/
    public static bool OtherProcess_3_3_4Enabled() {
        return (state == State._134);
    }
    [Action]
    public static void OtherProcess_3_3_4(){
        switch (state) {
            case State._134:
                state = State._60;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_3_4
    ***************************************************************************/
    public static bool OtherProcess_4_3_4Enabled() {
        return (state == State._60);
    }
    [Action]
    public static void OtherProcess_4_3_4(){
        switch (state) {
            case State._60:
                state = State._63;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_3_4
    ***************************************************************************/
    public static bool OtherProcess_5_3_4Enabled() {
        return (state == State._63);
    }
    [Action]
    public static void OtherProcess_5_3_4(){
        switch (state) {
            case State._63:
                state = State._143;
                break;
        }
    }
    
    /*************************************************************************
    Transition MainProcess_4
    ***************************************************************************/
    public static bool MainProcess_4Enabled() {
        return (state == State._143);
    }
    [Action]
    public static void MainProcess_4(){
        switch (state) {
            case State._143:
                state = State._44;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_1_4_5
    ***************************************************************************/
    public static bool OtherProcess_1_4_5Enabled() {
        return (state == State._44);
    }
    [Action]
    public static void OtherProcess_1_4_5(){
        switch (state) {
            case State._44:
                state = State._58;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_4_5
    ***************************************************************************/
    public static bool OtherProcess_2_4_5Enabled() {
        return (state == State._58);
    }
    [Action]
    public static void OtherProcess_2_4_5(){
        switch (state) {
            case State._58:
                state = State._47;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_4_5
    ***************************************************************************/
    public static bool OtherProcess_3_4_5Enabled() {
        return (state == State._47);
    }
    [Action]
    public static void OtherProcess_3_4_5(){
        switch (state) {
            case State._47:
                state = State._70;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_4_5
    ***************************************************************************/
    public static bool OtherProcess_4_4_5Enabled() {
        return (state == State._70);
    }
    [Action]
    public static void OtherProcess_4_4_5(){
        switch (state) {
            case State._70:
                state = State._7;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_5_4_5
    ***************************************************************************/
    public static bool OtherProcess_5_4_5Enabled() {
        return (state == State._7);
    }
    [Action]
    public static void OtherProcess_5_4_5(){
        switch (state) {
            case State._7:
                state = State._78;
                break;
        }
    }
    
    /*************************************************************************
    Transition MainProcess_5
    ***************************************************************************/
    public static bool MainProcess_5Enabled() {
        return (state == State._78);
    }
    [Action]
    public static void MainProcess_5(){
        switch (state) {
            case State._78:
                state = State._93;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_1_5_0
    ***************************************************************************/
    public static bool OtherProcess_1_5_0Enabled() {
        return (state == State._93);
    }
    [Action]
    public static void OtherProcess_1_5_0(){
        switch (state) {
            case State._93:
                state = State._21;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_2_5_0
    ***************************************************************************/
    public static bool OtherProcess_2_5_0Enabled() {
        return (state == State._21);
    }
    [Action]
    public static void OtherProcess_2_5_0(){
        switch (state) {
            case State._21:
                state = State._73;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_3_5_0
    ***************************************************************************/
    public static bool OtherProcess_3_5_0Enabled() {
        return (state == State._73);
    }
    [Action]
    public static void OtherProcess_3_5_0(){
        switch (state) {
            case State._73:
                state = State._6;
                break;
        }
    }
    
    /*************************************************************************
    Transition OtherProcess_4_5_0
    ***************************************************************************/
    public static bool OtherProcess_4_5_0Enabled() {
        return (state == State._6);
    }
    [Action]
    public static void OtherProcess_4_5_0(){
        switch (state) {
            case State._6:
                state = State._32;
                break;
        }
    }
    }public static class Factory {
public static ModelProgram Create() {
return new LibraryModelProgram(typeof(Factory).Assembly, "Tring5");
}
}
}