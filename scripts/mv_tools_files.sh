#!/bin/bash

models_folder="../results-1h"
logs_folder="../results"

mista_files=`find "${models_folder}" -name "*Test*.txt" -type f`

for f in `echo $mista_files`; do
  new_path="${f/$models_folder/$logs_folder}"

  echo "moving" "$f" " -> " $new_path;
  cp "$f" "$new_path";
done
