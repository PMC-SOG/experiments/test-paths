#!/bin/bash

# GreatSPN path
gspn="/Applications/GreatSPN.app/Contents/app/portable_greatspn/bin/GSPNRG"

# input file
input_file="/Users/himito/Work/code/pmc-sog/experiments/test-paths/models/sbus/SafeBus6"

# output folder
"$gspn" "${input_file}" "-dot-F" "${input_file}.dot"
