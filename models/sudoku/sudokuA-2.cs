using NModel;
using NModel.Attributes;
using NModel.Execution;
namespace Sudokua_2 {

public static class Sudokua_2 {
public enum State { _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34 };
public static State state = State._14;
    /*************************************************************************
    Transition select_0_0_0
    ***************************************************************************/
    public static bool select_0_0_0Enabled() {
        return (state == State._14) || (state == State._11) || (state == State._7) || (state == State._25) || (state == State._30) || (state == State._22) || (state == State._4) || (state == State._27) || (state == State._29);
    }
    [Action]
    public static void select_0_0_0(){
        switch (state) {
            case State._14:
                state = State._6;
                break;
            case State._11:
                state = State._28;
                break;
            case State._7:
                state = State._16;
                break;
            case State._25:
                state = State._18;
                break;
            case State._30:
                state = State._3;
                break;
            case State._22:
                state = State._32;
                break;
            case State._4:
                state = State._13;
                break;
            case State._27:
                state = State._5;
                break;
            case State._29:
                state = State._20;
                break;
        }
    }

    /*************************************************************************
    Transition select_0_0_1
    ***************************************************************************/
    public static bool select_0_0_1Enabled() {
        return (state == State._14) || (state == State._2) || (state == State._21) || (state == State._25) || (state == State._30) || (state == State._10) || (state == State._19) || (state == State._17) || (state == State._12);
    }
    [Action]
    public static void select_0_0_1(){
        switch (state) {
            case State._14:
                state = State._26;
                break;
            case State._2:
                state = State._8;
                break;
            case State._21:
                state = State._34;
                break;
            case State._25:
                state = State._1;
                break;
            case State._30:
                state = State._24;
                break;
            case State._10:
                state = State._9;
                break;
            case State._19:
                state = State._15;
                break;
            case State._17:
                state = State._33;
                break;
            case State._12:
                state = State._0;
                break;
        }
    }

    /*************************************************************************
    Transition select_0_1_0
    ***************************************************************************/
    public static bool select_0_1_0Enabled() {
        return (state == State._14) || (state == State._26) || (state == State._21) || (state == State._7) || (state == State._30) || (state == State._34) || (state == State._24) || (state == State._17) || (state == State._33);
    }
    [Action]
    public static void select_0_1_0(){
        switch (state) {
            case State._14:
                state = State._2;
                break;
            case State._26:
                state = State._8;
                break;
            case State._21:
                state = State._10;
                break;
            case State._7:
                state = State._23;
                break;
            case State._30:
                state = State._19;
                break;
            case State._34:
                state = State._9;
                break;
            case State._24:
                state = State._15;
                break;
            case State._17:
                state = State._12;
                break;
            case State._33:
                state = State._0;
                break;
        }
    }

    /*************************************************************************
    Transition select_0_1_1
    ***************************************************************************/
    public static bool select_0_1_1Enabled() {
        return (state == State._14) || (state == State._6) || (state == State._21) || (state == State._7) || (state == State._25) || (state == State._16) || (state == State._18) || (state == State._27) || (state == State._5);
    }
    [Action]
    public static void select_0_1_1(){
        switch (state) {
            case State._14:
                state = State._11;
                break;
            case State._6:
                state = State._28;
                break;
            case State._21:
                state = State._31;
                break;
            case State._7:
                state = State._22;
                break;
            case State._25:
                state = State._4;
                break;
            case State._16:
                state = State._32;
                break;
            case State._18:
                state = State._13;
                break;
            case State._27:
                state = State._29;
                break;
            case State._5:
                state = State._20;
                break;
        }
    }

    /*************************************************************************
    Transition select_1_0_0
    ***************************************************************************/
    public static bool select_1_0_0Enabled() {
        return (state == State._14) || (state == State._26) || (state == State._2) || (state == State._11) || (state == State._30) || (state == State._8) || (state == State._24) || (state == State._19) || (state == State._15);
    }
    [Action]
    public static void select_1_0_0(){
        switch (state) {
            case State._14:
                state = State._21;
                break;
            case State._26:
                state = State._34;
                break;
            case State._2:
                state = State._10;
                break;
            case State._11:
                state = State._31;
                break;
            case State._30:
                state = State._17;
                break;
            case State._8:
                state = State._9;
                break;
            case State._24:
                state = State._33;
                break;
            case State._19:
                state = State._12;
                break;
            case State._15:
                state = State._0;
                break;
        }
    }

    /*************************************************************************
    Transition select_1_0_1
    ***************************************************************************/
    public static bool select_1_0_1Enabled() {
        return (state == State._14) || (state == State._6) || (state == State._2) || (state == State._11) || (state == State._25) || (state == State._28) || (state == State._18) || (state == State._4) || (state == State._13);
    }
    [Action]
    public static void select_1_0_1(){
        switch (state) {
            case State._14:
                state = State._7;
                break;
            case State._6:
                state = State._16;
                break;
            case State._2:
                state = State._23;
                break;
            case State._11:
                state = State._22;
                break;
            case State._25:
                state = State._27;
                break;
            case State._28:
                state = State._32;
                break;
            case State._18:
                state = State._5;
                break;
            case State._4:
                state = State._29;
                break;
            case State._13:
                state = State._20;
                break;
        }
    }

    /*************************************************************************
    Transition select_1_1_0
    ***************************************************************************/
    public static bool select_1_1_0Enabled() {
        return (state == State._14) || (state == State._6) || (state == State._26) || (state == State._11) || (state == State._7) || (state == State._28) || (state == State._16) || (state == State._22) || (state == State._32);
    }
    [Action]
    public static void select_1_1_0(){
        switch (state) {
            case State._14:
                state = State._25;
                break;
            case State._6:
                state = State._18;
                break;
            case State._26:
                state = State._1;
                break;
            case State._11:
                state = State._4;
                break;
            case State._7:
                state = State._27;
                break;
            case State._28:
                state = State._13;
                break;
            case State._16:
                state = State._5;
                break;
            case State._22:
                state = State._29;
                break;
            case State._32:
                state = State._20;
                break;
        }
    }

    /*************************************************************************
    Transition select_1_1_1
    ***************************************************************************/
    public static bool select_1_1_1Enabled() {
        return (state == State._14) || (state == State._6) || (state == State._26) || (state == State._2) || (state == State._21) || (state == State._8) || (state == State._34) || (state == State._10) || (state == State._9);
    }
    [Action]
    public static void select_1_1_1(){
        switch (state) {
            case State._14:
                state = State._30;
                break;
            case State._6:
                state = State._3;
                break;
            case State._26:
                state = State._24;
                break;
            case State._2:
                state = State._19;
                break;
            case State._21:
                state = State._17;
                break;
            case State._8:
                state = State._15;
                break;
            case State._34:
                state = State._33;
                break;
            case State._10:
                state = State._12;
                break;
            case State._9:
                state = State._0;
                break;
        }
    }
    }public static class Factory {
public static ModelProgram Create() {
return new LibraryModelProgram(typeof(Factory).Assembly, "Sudokua_2");
}
}
}