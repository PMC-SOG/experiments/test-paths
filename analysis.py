#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import re
import csv
import glob
import pandas as pd
from collections import defaultdict
import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio
from bs4 import BeautifulSoup

# pandas precision
pd.set_option("display.precision", 3)

# timeout 60 minutes
timeout_m = 60

# timeout in miliseconds
TIMEOUT = timeout_m * 60 * 1000

# models to analyse
models = ["example", "philo", "referendum", "sbus", "servers", "smemory", "sudoku", "train", "tring"]

# csv with results
csv_filename = "results.csv"
sog_csv_filename = "sog_info.csv"

# folder with tools results
log_folder = "results"

# folder with the models
models_folder = "models"

# filenames
instance_file = "{model}/{instance}.pnml"
sog_file = "{model}/{instance}.res"
mista_file = "{model}/{instance}Tester_TC.txt"
nmodel_file = "{model}/{instance}ContractTest.txt"

# templates
tools_fieldnames = ["sogMBT(ms)", "MISTA(ms)", "NModel(ms)"]

pio.kaleido.scope.mathjax = None


# # Generate CSV file with data

# In[2]:


def format_unit(value, unit):
    if unit == "ms":
        return round(float(value),3)
    elif unit == "second" or unit == "seconds":
        return round(float(value)*1000,3)
    else:
        raise Exception(f"Unit {unit} is not supported") 


# In[3]:


def compute_stats(test_suite):
    nb_tests = len(test_suite)
    avg = sum([len(t) for t in test_suite]) / nb_tests * 1.0
    std = sum([(len(t) - avg) ** 2 for t in test_suite]) / nb_tests * 1.0
    return {"test_suite": test_suite, "n": nb_tests, "avg": avg, "std": std}

def get_mista_info(filename):
    tests = defaultdict(list)
    with open(filename, "r") as f:
        k = None
        for l in f.readlines():
            line = l.strip()
            # ignore comments
            if line.startswith("*") or line == "" or "seconds" in line:
                continue

            # a new test was found
            if line.startswith("test"):
                k = line
                continue

            tests[k].append(line)

        test_suite = tests.values()
        return compute_stats(test_suite)

def get_nmodel_info(filename):
    with open(filename, "r") as f:
        content = re.sub(r"[\n\t\s]*", "", f.read().replace("()", ""))
        tests_match = re.findall(r"TestCase\(([^()]+)\)", content)
        test_suite = [t.split(",") for t in tests_match]
        return compute_stats(test_suite)


# In[4]:


def generate_csv():
    regex_timeout = re.compile(r"timeout")
    regex_memory_out = re.compile(r"OutOfMemoryError")
    
    regex_sog = re.compile(r"Total time: (\d+\.?\d*) (\w+)")
    regex_obs = re.compile(r"# observable transitions: (\d+)")
    regex_paths = re.compile(r"# abstract paths: (\d+)")
    regex_sog_avg = re.compile(r"# of transitions per abstract path: (\d+\.?\d*)")
    regex_sog_std = re.compile(r"standard deviation: (\d+\.?\d*)")
    
    regex_mista = re.compile(r"Time for test code generation: (\d+\.?\d*) (\w+)")
    
    regex_nmodel = re.compile(r"real\s*(\d+m\d+,\d+s)")

    with open(csv_filename, "w") as csv_file:
        fieldnames = ['model', 'instance', 'places', 'transitions', 'arcs', 'obs. transitions', 'paths sogMBT', 'paths MISTA', 'paths NModel', 'avg. paths sogMBT', 'avg. paths MISTA', 'avg. paths NModel', 'std. paths sogMBT', 'std. paths MISTA', 'std. paths NModel']
        fieldnames += tools_fieldnames

        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for model in models:
            model_root_folder = os.path.join(models_folder, model)
            model_instances = [os.path.basename(f).rsplit('.', maxsplit=1)[0] for f in glob.glob(f"{model_root_folder}/*.net")]

            for instance in model_instances:
                instance_filename = instance_file.format(model=model, instance=instance)

                # get information of the model
                nb_places=""
                nb_transitions=""
                nb_arcs=""
                with open(os.path.join(models_folder, instance_filename), 'r') as m_file:
                    soup = BeautifulSoup(m_file, "xml")
                    nb_places = len(soup.find_all("place"))
                    nb_arcs = len(soup.find_all("arc"))
                    nb_transitions = len(soup.find_all("transition"))

                # get times for sogMBT
                sog_time = ""
                sog_std = ""
                sog_avg = ""
                nb_obs_t=""
                sog_nb_paths=""
                sog_filename = sog_file.format(model=model, instance=instance)
                with open(os.path.join(log_folder, sog_filename), 'r') as t_file:
                    sog_content = t_file.read()
                    # get time
                    if regex_timeout.search(sog_content) is None:
                        _sog, _sog_unit = regex_sog.search(sog_content).groups()
                        sog_time=f"{format_unit(_sog, _sog_unit)}"
                    
                    # number of observable transitions
                    obs_t_match = regex_obs.search(sog_content)
                    if obs_t_match is not None:
                        nb_obs_t = int(obs_t_match.group(1))
                        
                        
                    # number of abstract paths
                    paths_match = regex_paths.search(sog_content)
                    if paths_match is not None:
                        sog_nb_paths = int(paths_match.group(1))   
                        
                    # average of paths
                    avg_match = regex_sog_avg.search(sog_content)
                    if avg_match is not None:
                        sog_avg =  float(avg_match.group(1))
                        
                    # std of paths
                    std_match = regex_sog_std.search(sog_content)
                    if std_match is not None:
                        sog_std =  float(std_match.group(1))
                    
                # get times for MISTA
                mista_time = "MO"
                mista_std = ""
                mista_avg = ""
                mista_nb_paths = ""
                mista_filename = mista_file.format(model=model, instance=instance)
                mista_log = os.path.join(log_folder, mista_filename)
                # TODO: create files with memoryOut
                if (os.path.exists(mista_log)):
                    with open(mista_log, "r") as m_file:
                        mista_content = m_file.read()
                        if regex_memory_out.search(mista_content) is None:
                            _mista, _mista_unit = regex_mista.search(mista_content).groups()
                            mista_time=f"{format_unit(_mista, _mista_unit)}"
                            
                            mista_stats = get_mista_info(mista_log)
                            mista_std = mista_stats["std"]
                            mista_avg = mista_stats["avg"]
                            mista_nb_paths = mista_stats["n"]
                            
                # get times for NModel
                nmodel_time = ""
                nmodel_std = ""
                nmodel_avg = ""
                nmodel_nb_paths = ""
                nmodel_filename = nmodel_file.format(model=model, instance=instance)
                nmodel_log = os.path.join(log_folder, nmodel_filename)
                if (os.path.exists(nmodel_log)):
                    with open(nmodel_log, "r") as n_file:
                        nmodel_content = n_file.read()
                        if regex_timeout.search(nmodel_content) is None:
                            _nmodel = regex_nmodel.search(nmodel_content).groups()  
                            nmodel_time = str(pd.to_timedelta(_nmodel).total_seconds()[0].astype('float'))
                            
                            nmodel_stats = get_nmodel_info(nmodel_log)
                            nmodel_std = nmodel_stats["std"]
                            nmodel_avg = nmodel_stats["avg"]
                            nmodel_nb_paths = nmodel_stats["n"]
                            

                # save info
                writer.writerow({"model": model,
                                 "instance": instance,
                                 "places": nb_places,
                                 "transitions": nb_transitions,
                                 "arcs": nb_arcs,
                                 "obs. transitions": nb_obs_t,
                                 'paths sogMBT': sog_nb_paths,
                                 'avg. paths sogMBT': sog_avg,
                                 'std. paths sogMBT': sog_std,
                                 "sogMBT(ms)": sog_time,
                                 'paths MISTA': mista_nb_paths,
                                 'avg. paths MISTA': mista_avg,
                                 'std. paths MISTA': mista_std,
                                 "MISTA(ms)": mista_time,
                                 'paths NModel': nmodel_nb_paths,
                                 'avg. paths NModel': nmodel_avg,
                                 'std. paths NModel': nmodel_std,
                                 "NModel(ms)": nmodel_time
                                })


# In[5]:


def generate_sog_csv():
    # sog info
    regex_aggr = re.compile(r"NB NODES : (\d+)")
    regex_sog = re.compile(r"Total time: (\d+\.?\d*) (\w+)")
    regex_obs_trans_time = re.compile(r"Time for computing observable transitions: (\d+\.?\d*(?:e-?\w+)?)")
    regex_init_time = re.compile(r"Time for computing the net: (\d+\.?\d*(?:e-?\w+)?)")
    regex_obs_paths_time = re.compile(r"Time for computing observable paths: (\d+\.?\d*(?:e-?\w+)?)")
    regex_abs_paths_time = re.compile(r"Time for computing abstract paths: (\d+\.?\d*(?:e-?\w+)?)")
    
    with open(sog_csv_filename, "w") as csv_file:
        fieldnames = ['model', 'instance', 'aggregates', 'obs. transitions(ms)', 'initialization(ms)', 'obs. paths(ms)', 'abstract paths(ms)', "total(ms)"]

        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for model in models:
            model_root_folder = os.path.join(models_folder, model)
            model_instances = [os.path.basename(f).rsplit('.', maxsplit=1)[0] for f in glob.glob(f"{model_root_folder}/*.net")]

            for instance in model_instances:
                instance_filename = instance_file.format(model=model, instance=instance)
                
                nb_aggr=""
                sog_time = ""
                obs_trans_time=""
                init_time=""
                obs_paths_time=""
                ans_paths_time=""
                
                sog_filename = sog_file.format(model=model, instance=instance)
                with open(os.path.join(log_folder, sog_filename), 'r') as t_file:
                    sog_content = t_file.read()
                    
                    # get time
                    sog_time_match = regex_sog.search(sog_content)
                    if sog_time_match is not None:
                        _sog, _sog_unit = sog_time_match.groups()
                        sog_time=f"{format_unit(_sog, _sog_unit)}"
                    
                    # number of aggregates
                    aggr_match = regex_aggr.search(sog_content)
                    if aggr_match is not None:
                        nb_aggr = int(aggr_match.group(1))    

                    # time for observable transitions
                    obs_trans_time_match = regex_obs_trans_time.search(sog_content)
                    if obs_trans_time_match is not None:
                        obs_trans_time = format_unit(float(obs_trans_time_match.groups(1)[0]), "seconds")

                    # time for initialization
                    init_time_match = regex_init_time.search(sog_content)
                    if init_time_match is not None:
                        init_time = format_unit(float(init_time_match.groups(1)[0]),"seconds")

                    # time for observable paths
                    obs_paths_time_match = regex_obs_paths_time.search(sog_content)
                    if obs_paths_time_match is not None:
                        obs_paths_time = format_unit(float(obs_paths_time_match.groups(1)[0]), "seconds")

                    # time for abstract paths
                    abs_paths_time_match = regex_abs_paths_time.search(sog_content)
                    if abs_paths_time_match is not None:
                        abs_paths_time = format_unit(float(abs_paths_time_match.groups(1)[0]), "seconds")

                    # save info
                    writer.writerow({"model": model,
                                     "instance": instance,
                                     "aggregates": nb_aggr,
                                     "obs. transitions(ms)": obs_trans_time,
                                     "initialization(ms)": init_time,
                                     "obs. paths(ms)": obs_paths_time,
                                     "abstract paths(ms)": abs_paths_time,
                                     "total(ms)": sog_time
                                    })                              


# In[6]:


generate_csv()


# In[7]:


generate_sog_csv()


# # Analyse Data

# In[8]:


def save_html(df, name):
    df.to_html(os.path.join('images',f'{name}.html'))
    
def highlight_min(s, color): 
    new_s = pd.to_numeric(s, errors='coerce')
    min_s = new_s.min() 
    return  [color if cell == min_s else '' for cell in new_s] 

def highlight_null(s, color):
    null_values = ["TO", "MO"]
    return  [color if cell in null_values or pd.isna(cell) else '' for cell in s] 

def highlight_cell(df, latex=False, null_color="red", min_color="green"):
    color_fmt="color: {{{color}}}; bfseries: ;" if latex else 'color: {color};'
    return df.style.apply(highlight_min, axis=1, subset=tools_fieldnames, color=color_fmt.format(color=min_color)).apply(highlight_null, axis=1, color=color_fmt.format(color=null_color)).format(na_rep='TO') 


# In[9]:


df = pd.read_csv(csv_filename)
df = df.sort_values(by=["instance"]).set_index(["model", "instance"])
df


# In[10]:


df_times = df.drop(labels=["places", "arcs"], axis=1)
df_colored = highlight_cell(df_times)
save_html(df_colored, 'table-times')

df_colored


# In[11]:


df_model_info = df[["places", "transitions", "arcs"]]
df_model_info


# In[12]:


df_sog_info = pd.read_csv(sog_csv_filename)
df_sog_info = df_sog_info.sort_values(by=["instance"]).set_index(["model", "instance"])
df_sog_info


# # Plot

# In[13]:


def export_to_latex(df, filename, highlight=True, null_color="BrickRed", min_color="OliveGreen"): 
    base_style = highlight_cell(df, True, null_color, min_color) if highlight else df.style    
    #s = base_style.format_index("\\textbf{{{}}}", escape="latex", axis=1).hide(axis='index')
    
    return base_style.to_latex(os.path.join('images', f"{filename}.tex"), hrules=True)


# In[14]:


export_to_latex(df_times, "table-times")
export_to_latex(df_model_info, "table-params", False)
export_to_latex(df_sog_info, "table-sog", False)


# # SOG Information

# In[15]:


bar_df = df_sog_info.dropna().reset_index()
bar_df[['obs. transitions(ms)','initialization(ms)','obs. paths(ms)','abstract paths(ms)', "total(ms)"]] = bar_df[['obs. transitions(ms)','initialization(ms)','obs. paths(ms)','abstract paths(ms)',"total(ms)"]].div(bar_df['total(ms)'], axis=0) * 100.
bar_df.columns = bar_df.columns.str.replace(r"ms", "%")
bar_df


# In[16]:


fig = px.bar(bar_df, 
             x="instance", 
             y=["obs. transitions(%)","initialization(%)","obs. paths(%)", "abstract paths(%)"],
             labels={'variable':"algorithm step"},
             template="simple_white")

# individual updates
patterns = ['-', '/',  'x', '\\', '|', '+', '.']
for i, bar in enumerate(fig.data):
    bar.marker.pattern.shape = patterns[i]
    
fig.update_yaxes(title="Percentage in relation to total time (%)")
fig.update_traces(marker=dict(color="black", line_color="black", pattern_fillmode="replace"))

fig.write_image(os.path.join("images","sog-times.pdf"))

fig


# In[ ]:




