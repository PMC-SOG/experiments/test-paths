#!/bin/bash

# C# compiler path
csc="csc"

# NModel library
nmodel="/Users/himito/Work/code/pmc-sog/experiments/test-paths/tools/NModel.dll"

# input file
input_file="/Users/himito/Work/code/pmc-sog/experiments/test-paths/models/tring/tring5.cs"

"$csc" "/t:library" "/r:${nmodel}" "${input_file}"
